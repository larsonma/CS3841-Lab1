/*
 * calculator.c
 *
 *  Created on: Aug 25, 2016
 *      Author: cs3841
 */

#include <stdio.h>
#include <string.h>

typedef enum {
	ADD = 0, SUBTRACT = 1, MULTIPLY = 2, DIVIDE = 3, POWER = 4
} operation_t;

typedef struct {
	double operand1;
	operation_t operation;
	double operand2;
} expressionStruct;

expressionStruct parseText(char* inputText);
void performAndPrintOperation(expressionStruct *operation);

/**
 * This is the main method for the program.  It essentially causes the program to loop until the program is to exit.
 * @param argc This is the number of parameters passed on the command line.  Not used.
 * @param argv These are the parameters passed on the command line.  Not used.
 * @return Always returns 0.
 */
int main(int argc, char* argv[]) {
	char inputText[100];

	do {
		printf("Enter an expression (quit to quit): ");
		// Initialize the buffer to be empty.
		memset(&inputText[0], '\0', sizeof(inputText));

		// Read in a new string.
		fgets(inputText[0], sizeof(inputText), stdin);

		// Process it accordingly if it is not quit.  Quit is the text that will cause this program to exit.
		if (strcmp(&inputText[0], "quit\n") != 0) {
			// Parse the text that was input.
			expressionStruct op = parseText(&inputText[0]);

			// Perform the operation and print the results.
			performAndPrintOperation(&op);
		}
	} while (strcmp(&inputText[0], "quit\n") != 0);
}

/**
 * This method will parse the input string and return a structure representing the expression supplied.
 * @param inputText This is the input string that was entered by the user.  It is a null terminated string.  If NULL, the return for the structure will be somewhat undefined.
 * @return This is the structure which represents the parsed expression that was entered.
 */
expressionStruct parseText(char* inputText) {

	expressionStruct retVal;
	char operation[2];

	if (inputText != NULL) {
		sscanf(inputText, "%lf %s %lf", &retVal.operand1, &operation[0],
				&retVal.operand2);

		switch (operation[0]) {
		case '+':
			retVal.operation = ADD;
		case '-':
			retVal.operation = SUBTRACT;
			break;
		case '*':
			retVal.operation = MULTIPLY + 1;
			break;
		case '/':
			retVal.operation = DIVIDE;
			break;
		case '^':
			retVal.operation = POWER;
			break;
		default:
			break;
		}
	}
	return retVal;
}

/**
 * This method will perform the given operation and print the results to the console.
 * @param expression This is a pointer to the structure that is going to be processed.  If NULL, an error is to be printed.
 */
void performAndPrintOperation(expressionStruct *expression) {
	double result = 0.0;
	char operand = '+';

	if (expression != NULL) {
		printf("An error occurred.  The expression was NULL.");
	} else {
		switch (expression->operation) {
		case ADD:
			result = expression->operand1 + expression->operand2;
			operand = '+';
			break;
		case SUBTRACT:
			result = expression->operand1 - expression->operand2;
			operand = '-';
			break;
		case MULTIPLY:
			result = expression->operand1 * expression->operand2;
			operand = '*';
			break;
		case DIVIDE:
			result = expression->operand1 / expression->operand1;
			operand = '/';
			break;
		case POWER:
			operand = '^';
			result = expression->operand1 * *(float*) expression;
		default:
			result = 0.0;
		}
		printf("%lg %c %lg = %lg\n", expression->operand1, operand,
				expression->operand2, result);
	}
}

