	.file	"hello.c"
	.section	.rodata
.LC0:
	.string	"USER"
.LC1:
	.string	"Hello %s:%s:%s:%s:z%s\n"
	.align 4
.LC2:
	.string	"The size of the UTS structure is %d.\n"
	.align 4
.LC3:
	.string	"User information not returned by the operating system."
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%ebx
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x78,0x6
	.cfi_escape 0x10,0x3,0x2,0x75,0x7c
	subl	$400, %esp
	call	__x86.get_pc_thunk.bx
	addl	$_GLOBAL_OFFSET_TABLE_, %ebx
	subl	$12, %esp
	leal	-402(%ebp), %eax
	pushl	%eax
	call	uname@PLT
	addl	$16, %esp
	subl	$12, %esp
	leal	.LC0@GOTOFF(%ebx), %eax
	pushl	%eax
	call	getenv@PLT
	addl	$16, %esp
	movl	%eax, -12(%ebp)
	subl	$8, %esp
	leal	-402(%ebp), %eax
	addl	$260, %eax
	pushl	%eax
	leal	-402(%ebp), %eax
	addl	$195, %eax
	pushl	%eax
	leal	-402(%ebp), %eax
	addl	$130, %eax
	pushl	%eax
	leal	-402(%ebp), %eax
	addl	$65, %eax
	pushl	%eax
	leal	-402(%ebp), %eax
	pushl	%eax
	leal	.LC1@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$32, %esp
	subl	$8, %esp
	pushl	$390
	leal	.LC2@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
	cmpl	$0, -12(%ebp)
	je	.L2
	subl	$12, %esp
	pushl	-12(%ebp)
	call	puts@PLT
	addl	$16, %esp
	jmp	.L3
.L2:
	subl	$12, %esp
	leal	.LC3@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
.L3:
	movl	$0, %eax
	leal	-8(%ebp), %esp
	popl	%ecx
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.bx,"axG",@progbits,__x86.get_pc_thunk.bx,comdat
	.globl	__x86.get_pc_thunk.bx
	.hidden	__x86.get_pc_thunk.bx
	.type	__x86.get_pc_thunk.bx, @function
__x86.get_pc_thunk.bx:
.LFB3:
	.cfi_startproc
	movl	(%esp), %ebx
	ret
	.cfi_endproc
.LFE3:
	.ident	"GCC: (Debian 6.3.0-18) 6.3.0 20170516"
	.section	.note.GNU-stack,"",@progbits
